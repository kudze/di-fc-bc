<?php

require_once __DIR__ . '/vendor/autoload.php';

function loadFromFile(string $file): array
{
    $contents = file_get_contents($file);
    if ($contents === false)
        throw new Exception("$file does not exist!");

    [$comment, $contents] = explode("1) Rules\n", $contents, 2);
    [$rules, $contents] = explode("2) Facts\n", $contents, 2);
    [$facts, $goal] = explode("3) Goal\n", $contents, 2);

    //We remove comments from rules
    $rules = trim($rules, "\n");
    if ($rules === '')
        $rules = [];
    else
        $rules = array_map(
            function (string $row) {
                $row = trim(explode('//', $row)[0]);
                $predicates = explode(' ', $row);

                $consequent = array_shift($predicates);
                $antecedent = $predicates;

                return [$antecedent, $consequent, false, false];
            },
            explode("\n", $rules)
        );

    return [$rules, explode(' ', trim($facts, "\n")), trim($goal, "\n")];
}

function printDataTrace(array $gdb, array $state, string $goal): void
{
    echo "PART 1. Data\n\n";

    echo "  1) Rules\n";
    $idx = 1;
    foreach ($gdb as [$antecedent, $consequent])
        echo "     R" . ($idx++) . ") " . implode(', ', $antecedent) . " -> $consequent\n";

    echo '  2) Facts ' . implode(', ', $state) . ".\n";
    echo "  3) Goal $goal.\n";

}

function printResults(?array $path, string $goal)
{
    echo "\nPART 3. Results\n";
    if ($path === null) {
        echo "  1) Goal $goal not achieved.\n";
        return;
    } else if (empty($path)) {
        echo "  Goal $goal in facts. Empty path.\n";
        return;
    }

    echo "  1) Goal $goal achieved.\n";
    echo "  2) Path R" . implode(', R', $path) . ".\n";
}