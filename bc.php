<?php

require_once __DIR__ . '/common.php';

function printTraceLineStart(int &$iteration, array $stack)
{
    $goal = getCurrentGoalFromStack($stack);
    echo str_pad(++$iteration, 2, ' ', STR_PAD_LEFT) . ") "
        . str_repeat('-', count($stack) - 1) . "Goal $goal. ";
}

function printPresentlyInferredFact(array $state)
{
    echo "Fact (presently inferred). Facts " . implode(' and ', $state) . ".";
}

function isCycle(array $stack, string $goal): bool
{
    //We ignore current el.
    array_pop($stack);

    foreach ($stack as [$goals, $state, $currGoalIndex]) {
        if ($goals[$currGoalIndex] === $goal)
            return true;
    }

    return false;
}

function isEarlierInferred(array $stack, string $goal): bool
{
    //We ignore current el.
    [, $state] = array_pop($stack);

    if (in_array($goal, $state))
        return true;

    foreach ($stack as [$goals, $state, $currGoalIndex]) {
        if (in_array($goal, $state))
            return true;

        //TODO: check if this is needed.
        for ($i = 0; $i < $currGoalIndex; $i++)
            if ($goals[$i] === $goal)
                return true;
    }

    return false;
}

function getCurrentGoalFromStackElement(array $stackElement): string
{
    $goal = $stackElement[0][$stackElement[2]];
    return $goal;
}

function getCurrentGoalFromStack(array $stack): string
{
    $lastStackIdx = array_key_last($stack);
    return getCurrentGoalFromStackElement(getLastElementFromStack($stack));
}

function getLastElementFromStack(array $stack): array
{
    $lastStackIdx = array_key_last($stack);
    return $stack[$lastStackIdx];
}

function backwardChainInner(
    array $gdb,
    array &$stack,
    array $facts,
    int &$iteration = 0,
    array $path = [],
): ?array {
    $currentStackEl = $stack[array_key_last($stack)];
    [$goals, $state, $currGoalIndex] = $currentStackEl;
    $goal = $goals[$currGoalIndex];

    //We check for cycles.
    if (isCycle($stack, $goal)) {
        printTraceLineStart($iteration, $stack);
        echo "Cycle. Back, FAIL.\n";
        return null;
    }

    //If goal in facts we can return.
    if (in_array($goal, $facts)) {
        printTraceLineStart($iteration, $stack);
        echo "Fact (initial), as facts are " . implode(' and ', $facts) . ". Back, OK.\n";
        return $path;
    }

    if (isEarlierInferred($stack, $goal)) {
        printTraceLineStart($iteration, $stack);
        //TODO: fix print.
        echo "Fact (earlier inferred), as facts " . implode(' and ', $state) . ". Back, OK.\n";
        return $path;
    }

    $anyRules = false;

    //Otherwise we need to find first rule 
    $ruleIndex = 0;
    foreach ($gdb as [$antecedent, $consequent]) {
        $ruleIndex++;

        if ($consequent !== $goal)
            continue;

        $anyRules = true;

        //Add found rule to stack.
        $orgPath = $path;
        $path[] = $ruleIndex;
        printTraceLineStart($iteration, $stack);
        echo "Find R" . ($ruleIndex) . ':' . implode(',', $antecedent) . "->$consequent. New goals " . implode(', ', $antecedent) . ".\n";
        $stack[] = [$antecedent, $state, 0];

        do {
            $_path = backwardChainInner($gdb, $stack, $facts, $iteration, $path);

            if ($_path !== null) {
                $path = $_path;
                [$_goals, $_state, $_currGoalIndex] = array_pop($stack);
                $_goal = getCurrentGoalFromStackElement([$_goals, $_state, $_currGoalIndex]);
                if (!in_array($_goal, $_state))
                    $_state[] = $_goal;

                //Need to handle the case where top one is not finished.
                if ($_currGoalIndex < count($_goals) - 1) {
                    $_currGoalIndex++;

                    $stack[] = [$_goals, $_state, $_currGoalIndex];
                    continue;
                }

                printTraceLineStart($iteration, $stack);

                $__state = $_state;
                [$_goals, $_state, $_currGoalIndex] = array_pop($stack);
                $_goal = $_goals[$_currGoalIndex];
                $_state = array_unique(array_merge($_state, $__state, [$_goal]));

                printPresentlyInferredFact($_state);
                if ($_currGoalIndex === count($_goals) - 1) {
                    if (count($stack) !== 0)
                        echo " Back,";
                    echo " OK.";
                }
                echo "\n";

                $stack[] = [$_goals, $_state, $_currGoalIndex];
                return $_path;
            } else {
                array_pop($stack);
                $path = $orgPath;
            }
        } while ($_path !== null);
    }

    printTraceLineStart($iteration, $stack);
    if (!$anyRules)
        echo "No rules. Back, FAIL.\n";
    else {
        echo "No more rules. Back, FAIL.\n";
    }

    return null;
}

function backwardChain(array $gdb, array $state, string $goal): ?array
{
    $iteration = 0;
    $stack = [[[$goal], $state, 0]];

    $path = backwardChainInner($gdb, $stack, $state, $iteration);

    if ($path === null)
        return null;

    return array_reverse($path);
}

function main(string $inputFileName): void
{
    [$gdb, $state, $goal] = loadFromFile($inputFileName);

    printDataTrace($gdb, $state, $goal);

    echo "\nPART 2. Trace\n";
    $path = backwardChain($gdb, $state, $goal);

    printResults($path, $goal);
}

if ($argc !== 2)
    exit("php bc.php <failas>\n");

if (!file_exists($argv[1]))
    exit("File \"{$argv[1]}\" does not exist!\n");

main($argv[1]);