<?php

require_once __DIR__ . '/common.php';

function forwardChain(array $gdb, array $initialState, string $goal): ?array
{
    echo "\nPART 2. Trace\n";

    if (in_array($goal, $initialState)) {
        echo "  Goal $goal in facts.\n";
        return [];
    }

    $path = [];
    $state = $initialState;
    $addedStates = [];

    $iteration = 1;
    do {
        $changed = false;
        echo "\n  ITERATION " . ($iteration++) . "\n";

        $ruleIndex = 1;
        foreach ($gdb as [$antecedent, $consequent, &$flag1, &$flag2]) {
            echo "    R" . ($ruleIndex++) . ':' . implode(',', $antecedent) . '->' . $consequent . ' ';

            if ($flag1) {
                echo "skip, because flag1 raised.\n";
                continue;
            }

            if ($flag2) {
                echo "skip, because flag2 raised.\n";
                continue;
            }

            $lacking = null;
            if (!canAntecedentBeAppliedToState($state, $antecedent, $lacking)) {
                echo "not applied, because of lacking $lacking.\n";
                continue;
            }

            if (in_array($consequent, $state)) {
                echo "not applied, because RHS in facts. Raise flag2.\n";
                $flag2 = true;
                continue;
            }

            $state[] = $consequent;
            $addedStates[] = $consequent;
            $path[] = $ruleIndex - 1;
            echo "apply. Raise flag1. Facts " . implode(', ', $initialState) . " and " . implode(', ', $addedStates) . ".\n";
            $flag1 = true;
            $changed = true;

            if ($consequent !== $goal)
                break;

            echo "    Goal achieved.\n";
            return $path;
        }
    } while ($changed);

    return null;
}

function canAntecedentBeAppliedToState(array $state, array $antecedent, ?string &$lacking): bool
{
    foreach ($antecedent as $predicate)
        if (!in_array($predicate, $state)) {
            $lacking = $predicate;
            return false;
        }

    return true;
}

function main(string $inputFileName): void
{
    [$gdb, $state, $goal] = loadFromFile($inputFileName);

    printDataTrace($gdb, $state, $goal);

    $path = forwardChain($gdb, $state, $goal);
    printResults($path, $goal);
}

if ($argc !== 2)
    exit("php fw.php <failas>\n");

if (!file_exists($argv[1]))
    exit("File \"{$argv[1]}\" does not exist!\n");

main($argv[1]);


